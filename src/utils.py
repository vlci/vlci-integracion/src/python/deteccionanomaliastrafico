# Código creado por Arturo Sirvent (2023) para el proyecto Smart City Valencia

# Utils para crear la deteccion de anomalías en tablas

# librerías

import traceback
import smtplib
from email.mime.multipart import MIMEMultipart
from email.utils import formataddr
from email.mime.text import MIMEText

import pandas as pd
from datetime import datetime, timedelta,timezone
from sqlalchemy import create_engine
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.engine.reflection import Inspector

# ======================== definición de clases ========================


class StatTest:
    # Clase que representa la media y la std de un conjunto de datos,
    #  además proporciona la Z.score cuando hacemos predict sobre un nuevo valor.
    # Se inicializa vacía, y se le da valores mediante el método de entrenamiento (predict).
    def __init__(self) -> None:
        self.std = None
        self.mean = None

    def fit(self, x_values):
        # le entregamos los valores de la ventana que tomamos como referencia estadística
        # los datos sin normalizar por supuesto
        self.std = x_values.std(skipna=True) if x_values.std(skipna=True) > 1e-2 else 1

        self.mean = x_values.mean(skipna=True)

    def predict(self, value_or_values):
        # si nos entregan varios valores, evaluamos todos en funcion de la estadistica aprendida
        # puede ser un series de pandas o un array

        y = (value_or_values-self.mean)/self.std
        return y


class EvalTramo_Zscore:
    # clase para evaluar la zscore de un tramo entero mediante una ventana movil.
    def __init__(self, df, eval_window=7*4, stride=1, max_lags=3) -> None:
        # El pasado usado para la evaluación está fijado en 4 semanas ( 4 x 7 ).
        # El stride es el avance de la ventana en cada evaluación. Debe ser 1, sino no se evaluarian todos los puntos.
        # Max_lags es la cantidad de zscores futuras se calculan con las estadísticas pasadas. Es decir, dado unas estadísticas de las últimas
        # 4 semanas, calculo las zscores de los "max_lags" días futuros. Esto es útil porque estamos evaluando la rareza de los datos no solo en
        # función de los días absolutamente sucesivos, resultando en una detección de cambios anómalos, pero +o- progresivos.
        self.df = df["value"]
        self.length = df.shape[0]
        self.max_lags = max_lags
        self.stride = stride
        self.eval_window = eval_window

        self.calc_zscores()

    def calc_zscores(self):
        # Recorremos la series, y vamos registrando las z scores
        future_zscore_list = []

        # el bucle va a empezar para eval_window despues de la primera fecha, y max_lags antes de la ultima
        aux_start_date = self.df.index.min()+timedelta(days=self.eval_window)
        aux_end_date = self.df.index.max()-timedelta(days=self.max_lags)

        # aqui suponemos una frecuencialidad diaria
        dates_end_intervals = pd.date_range(aux_start_date, aux_end_date, freq=f"{int(self.stride)}D", tz='UTC')

        # iteramos entre las ventanas que vamos a evaluar
        for i in dates_end_intervals:

            aux_date_start_train = i-timedelta(days=self.eval_window)
            aux_date_end_train = i
            aux_date_start_test = i+timedelta(days=1)
            aux_date_end_test = i+timedelta(days=int(self.max_lags))

            series_aux = self.df.loc[(self.df.index >= aux_date_start_train) & (self.df.index <= aux_date_end_train)]
            future_aux = self.df.loc[(self.df.index >= aux_date_start_test) & (self.df.index <= aux_date_end_test)]

            if (series_aux.size > 0) & (future_aux.size > 0):
                model = StatTest()
                model.fit(series_aux)
                # ya tenemos el modelo con los parámetros que debe, ahora tenemos que sacar las z scores futuras
                future_zscore = model.predict(future_aux)
                # Convertir la serie en un DataFrame
                future_zscore = future_zscore.to_frame()

                # Añadir una columna llamada "posicion" con valores desde 1 hasta X
                future_zscore['lag_view'] = range(1, len(future_zscore) + 1)
                future_zscore_list.append(future_zscore)

        # cuando termina lo juntamos todo
        if len(future_zscore_list) > 0:
            self.final_zscore = pd.concat(future_zscore_list)
            self.final_zscore = self.final_zscore.pivot(columns="lag_view", values="value")
        else:
            self.final_zscore = None

    def classify(self, thresh, type="mean"):
        # el umbral tendrá que ser establecido a "mano", segun el nivel de confianza que queremos. Si establecemos 3, estaremos considerando desviaciones máximas de 3 sigmas
        # que corresponden al 99% de los datos. Esto trabajando con la media por filas del df obtenido (donde cada columa es la zscore a diferente lag.)

        # otra opcion es usar, en lugar de la media sobre las filas, la std. Esto da un perfil más claro sobre las anomalías, pero no se interpretarlo bien
        # y eso puede ser un problema para establecer los intervalos.
        if self.final_zscore is not None:
            if type == "mean":
                self.is_anom_df_aux = self.final_zscore.mean(axis=1, skipna=True)

            elif type == "std":
                self.is_anom_df = self.final_zscore.std(axis=1, skipna=True)

            else:
                raise Exception
            
            self.is_anom_df=self.is_anom_df_aux.copy()
            # el orden es muy imporante en esta operacion, sino todo ira a 0!!!! 
            self.is_anom_df.loc[(self.is_anom_df_aux.abs() <= thresh) & (self.is_anom_df_aux >= 0)] = 0  
            self.is_anom_df.loc[(self.is_anom_df_aux.abs() > thresh) & (self.is_anom_df_aux >= 0)] = 1  

            self.is_anom_df.loc[(self.is_anom_df_aux.abs() <= thresh * 0.8) & (self.is_anom_df_aux < 0)] = 0  
            self.is_anom_df.loc[(self.is_anom_df_aux.abs() > thresh * 0.8) & (self.is_anom_df_aux < 0)] = 1  
            return self.is_anom_df
        else:
            return None


class Email:
    # clase para el email de notificación para cuando hay detecciones nuevas
    def __init__(self, secrets, config_tables2check) -> None:
        # recibimos los secretos para la conexión, y la info de las tablas to check, para tener el nombre de las tablas de
        # las que provienen los avisos
        self.secrets = secrets
        self.config_tables2check = config_tables2check

    def send_email(self, subject, body):
        # esto hace todo lo necesario para el envio del email
        msg = MIMEMultipart()
        msg['From'] = formataddr((self.secrets['email_sendername'], self.secrets['email_username']))
        msg['To'] = self.secrets['email_receiver']
        msg['Subject'] = subject
        msg.attach(MIMEText(body, 'plain'))

        try:
            server = smtplib.SMTP(self.secrets['smtp_server'], self.secrets['smtp_port'])
            server.starttls()
            server.login(self.secrets['email_username'],self.secrets['email_password'])
            text = msg.as_string()
            server.sendmail(self.secrets['email_username'],self.secrets['email_receiver'], text)
            server.quit()
            print("Correo enviado exitosamente.")
        except Exception as e:
            print(f"Error al enviar el correo: \n {traceback.print_exc()}")

    def write_message_from_df(self, df):
        # este método es para confeccionar el email de aviso.
        assert isinstance(
            df, pd.DataFrame) and not df.empty, "Error con el df notify para componer el mensaje en write_message_from_df()"

        text_init = f"Potenciales anomalías detectadas a fecha: {datetime.now().strftime('%d/%m/%Y')}: \n \n \n"
        lista_mensajes_all_tables = []
        # lo ideal aqui sería poner un groupby por el measureunitcas, porque si un mismo tramo esta para ambos todo sale aqui
        for name_0, data_0 in df.groupby("tables2check", group_keys=False):
            lista_mensajes_all_ids = [f"Anomalías en: {self.config_tables2check[name_0]['identificador_email']} \n"]
            for name, data in data_0.groupby("id"):
                array_fechas = data.sort_index().index.date
                if len(array_fechas) > 0:
                    motivos_array = data["motivo"].to_numpy()
                    assert motivos_array.size == array_fechas.size, f"El tamaño del array de fechas y el de motivos no es el mismo. Fechas: {array_fechas.shape}, Motivos: {motivos_array.shape}"
                    init_date = array_fechas[0]
                    end_date = array_fechas[0]
                    lista_mensajes = [f" {'='*15} \n ID: {name} \n "]
                    for i, date in enumerate(array_fechas):
                        # recorremos el df de fechas y vamos guardando un mensaje para cada rango de fechas que consideramos relevantes
                        try:
                            # si no se trata del ultimo registro palante
                            if i < (len(array_fechas)-1):
                                # IMPORTANTE, AQUI HACEMOS LA COMPARACION DE DATES. SI AL FINAL QUISIERAMOS REVISAR POR HORAS O LO QUE SEA, HABIA QUE LLEVAR UN REGISTRO MÁS GRANULADO
                                if (date+timedelta(days=1)) == array_fechas[i+1]:
                                    end_date = array_fechas[i+1]
                                else:
                                    # si no sigue el tramo de fechas, terminamos y confeccionamos el mensaje
                                    if init_date == end_date:
                                        lista_mensajes.append(f"Ha habido una alerta tipo {motivos_array[i]},  el dia {init_date.strftime(' %d/%m/%Y')}")
                                    else:
                                        lista_mensajes.append(f"Ha habido una alerta tipo {motivos_array[i]}, en el rango de fechas {init_date.strftime('%d/%m/%Y')} -->  {end_date.strftime('%d/%m/%Y')} ")
                                    init_date = array_fechas[i+1]
                                    end_date = array_fechas[i+1]
                            else:
                                # si es el ultimo registro entonces ponemos ese como el ultimo
                                end_date = date
                        except Exception as err:
                            print(f"Error confeccionando el mensaje del ID: {name}, para{date.strftime('%d/%m/%Y')} , {traceback.print_exc()}")
                    try:
                        # al final tambien lo tenemos que poner
                        if init_date == end_date:
                            lista_mensajes.append(f"Ha habido una alerta tipo {motivos_array[i]}, el dia {init_date.strftime(' %d/%m/%Y')}")
                        else:
                            lista_mensajes.append(f"Ha habido una alerta tipo {motivos_array[i]}, en el rango de fechas {init_date.strftime('%d/%m/%Y')} -->  {end_date.strftime('%d/%m/%Y')} ")
                        # y despues de tener todos los mensajes, tenemos que juntarlo
                        mensaje_final = " \n " + " \n \n ".join(lista_mensajes)
                        lista_mensajes_all_ids.append(mensaje_final)
                    except Exception as err:
                        print(f"Error confeccionando el mensaje final, {traceback.print_exc()}")
                else:
                    print(f"Para el ID: {name} no hay fechas al escribir el mensaje")
            lista_mensajes_all_tables.append("\n ".join(lista_mensajes_all_ids))
            procedimiento_frente_anomalia = """
Este aviso se ha producido por la detección NUEVA de una/s anomalía/s.  
El procedimiento es el siguiente:  

    1. Investigar la anomalía, corregirla si es posible (con todo el procedimento de sqitch etc.), o determinar que no es posible corregirla. Hay 3 tipos de anomalías detectables:  
        a. Nans: Cuando falta un registro para un día (se asume que hay registros diarios para todos los tramos y para bicicletas y vehículos).  
        b. ValoresRepetidos: Ocurre cuando tenemos más de un registro en un día (se asume un máximo de un registro diario).  
        c. Umbral: Cuando el valor registrado es muy diferente al resto de datos recientes. Si un valor es muy diferente al pasado reciente, significa un cambio brusco de los valores típicos que estaba tomando ese tramo. Esto puede ser totalmente normal, y puede no ser una anomalía a corregir (con este procedimiento se pretenden corregir las anomalías técnicas, pero si de verdad hubo un registro anómalo, debemos dejarlo como estaba). 

    2. Una vez se ha investigado la anomalía, y queremos que aparezca como 'Done', vamos a la carpeta en la M.Talend (/opt/etls/sc_vlci/[PRE|PRO]/py_oci_trafico_deteccion_anomalias/data), y en el fichero data/revisar.csv, eliminamos la fila del registro que hemos revisado. Esto hará que se etiquete como 'Done' en la tabla: t_datos_python_trafico_deteccionanomalias de vlci2.  
    
    Nota: Si la anomalía ha sido corregida y el algoritmo ya no la detecta como anomalía, ya no aparecerá ni como anomalía corregida, porque no será detectada. \n \t Esto es porque se ejecuta la detección de anomalías CADA VEZ, sobre todos los datos, después se observa el registro y si hay nuevas se avisa, y si algunas han sido corregidas, se olvidan.   

            """
        return text_init + " \n \n =========================== \n ".join(lista_mensajes_all_tables) + " \n \n =========================== \n \n " + procedimiento_frente_anomalia


# ============================ definición de funciones  ================================

def read_from_db(secretos, query):
    # funcion para lectura de la base de datos
    list_chunks = []

    # Crear la cadena de conexión
    connection_string = f"postgresql://{secretos['username']}:{secretos['password']}@{secretos['host']}:{secretos['port']}/{secretos['dbname']}"
    # Crear el motor de SQL Alchemy
    engine = create_engine(connection_string, connect_args={'options': '-csearch_path={}'.format(secretos['schema'])})

    # Leemos los datos de la base de datos
    for chunk in pd.read_sql_query(query, engine, chunksize=2000):
        # Aquí se procesa cada fragmento de datos. Puedes adaptarlo a tus necesidades.
        # Por ahora, simplemente imprimimos los datos.
        list_chunks.append(chunk)
    df = pd.concat(list_chunks)
    return df


def write_to_db(df,secrets,db_tablename):
    # Función que recibe el df, crea la conexión y lo guarda
    connection_string = f"postgresql://{secrets['username']}:{secrets['password']}@{secrets['host']}:{secrets['port']}/{secrets['dbname']}"
    # Crear el motor de SQL Alchemy
    engine = create_engine(connection_string, connect_args={'options': '-csearch_path={}'.format(secrets['schema'])})

    #Antes de escribir nada importante, vamos a escribir una prueba, porque por alguna razon esta fallando algunas veces el metodo .to_sql()  
    inspector = Inspector.from_engine(engine)
    # Verifica si la tabla existe en la base de datos
    if inspector.has_table(db_tablename):
        # Realiza la acción que deseas si la tabla existe
        df_aux=pd.DataFrame([{'time': datetime(2023, 1, 11, 0, 0, tzinfo=timezone.utc), 'motivo': 'Test', 'id': 'Test', 'tables2check': 'trafico_bicis', 'status': 'Done'},
                                {'time': datetime(2023, 1, 12, 0, 0,tzinfo=timezone.utc), 'motivo': 'Test', 'id': 'Test', 'tables2check': 'trafico_vehiculos', 'status': 'Done'}]).set_index("time")
        try:    
            df_aux.to_sql(db_tablename, engine, if_exists='append', index=True)
        except Exception as err:
            print(f"Unexpected error on test INSERT INTO. {type(err)=}")
            raise
        finally:
            engine.dispose()

        #Ahora lo hacemos de nuevo sabiendo que funciona la insercion de datos

        # Crear el motor de SQL Alchemy
        engine = create_engine(connection_string, connect_args={'options': '-csearch_path={}'.format(secrets['schema'])})
        # Vaciar el contenido de la tabla
        with engine.connect() as connection:
            try:
                connection.execute(f"DELETE FROM {db_tablename}")
            except SQLAlchemyError as e:
                print(f"Error al vaciar la tabla: {str(e)}")
                # Aquí puedes manejar el error de la forma que prefieras, por ejemplo, notificarlo o tomar alguna acción específica.
            except Exception as err:
                print(f"Unexpected error on DELETE FROM. {type(err)=}")
                raise

        # Volcar el DataFrame que realmente nos interesa a la base de datos
        try:
            df.to_sql(db_tablename, engine, if_exists='append', index=True)
        except Exception as err:
            print(f"Unexpected {err=}, {type(err)=}")
            raise
        finally:
            engine.dispose()
    else:
        # Realiza una acción alternativa si la tabla no existe
        print("La tabla no existe en la base de datos.")
        engine.dispose()


def procesado_datos(df):
    # procesar los datos recien recibidos de la BBDD, para luegar hacer la detección de anomalías
    df["time"] = pd.to_datetime(df["time"], utc=True)

    # vamos a forzar a que solo haya un registro por día, y que esten todos los días, y luego ya comprobamos si esta todo
    # si hay varios se hace la media, y devolvemos la cuenta de cuantos hay
    df_one_per_day = df.groupby(by=[df["time"].dt.date, "id"]).agg({"value": ["mean", "count"]})
    df_one_per_day.columns = df_one_per_day.columns.droplevel()
    df_one_per_day = df_one_per_day.reset_index()
    df_one_per_day = df_one_per_day.rename(columns={"mean": "value"})
    # sacamos los IDs individuales
    ID_list = df_one_per_day["id"].unique()

    # hacemos el bucle para justar todos los IDs
    all_series = []
    for id in ID_list:
        df_aux = df_one_per_day.loc[df_one_per_day["id"] == id].set_index("time")
        df_aux.index = pd.to_datetime(df_aux.index, utc=True)
        df_aux2 = df_aux.join(pd.DataFrame(pd.date_range(df_aux.index.min(), df_aux.index.max(), tz='UTC'), columns=["time"]).set_index("time"), how="outer")
        df_aux2.loc[:, "id"] = id
        aux1 = df_aux2[["id", "value", "count"]]
        # aux1.name=trm
        all_series.append(aux1)

    df_all_values = pd.concat(all_series, axis=0)

    # le añadimos una columna para que sea facil la identificacion de los nans
    df_all_values["is_nan"] = 0
    df_all_values.loc[df_all_values["value"].isna(),"is_nan"] = 1
    return df_all_values


def detect_id_main(data_id, name, model, **kargs):
    # después del procesado está es la función que hace la detección
    # en esta funcion devolvemos un df con indices las fechas de las anomalías, y en la otra columna, el motivo
    assert data_id.size > 0, f"Tamaño del df para el id {name}, df --> {data_id}"
    aux_lista_notify = []
    # comprobación de nanns
    df_with_nans = data_id.loc[data_id["is_nan"] == 1]
    if df_with_nans.size != 0:
        # solo lo introducimos
        aux_lista_notify.append(pd.DataFrame(data="Nans", index=df_with_nans.index, columns=["motivo"]))
    # comprobación de valores repetidos
    df_with_repeated = data_id.loc[(data_id["count"] != 1) & (data_id["count"].notnull())]
    if df_with_repeated.size != 0:
        # solo lo introducimos
        aux_lista_notify.append(pd.DataFrame(data="ValoresRepetidos", index=df_with_repeated.index, columns=["motivo"]))
    # Comprobación de los umbrales
    eval = model(data_id)
    aux_resultado = eval.classify(**kargs)  # , type="mean")
    if aux_resultado is not None:
        aux_resultado = aux_resultado.loc[aux_resultado == 1]
        if aux_resultado.size > 0:
            aux_lista_notify.append(pd.DataFrame(data="Umbral", index=aux_resultado.index, columns=["motivo"]))

        # y si tenemos registro que juntar, los juntamos
    if len(aux_lista_notify) > 0:
        aux_df_notify_tramo = pd.concat(aux_lista_notify).groupby(level=0).apply(lambda x: " | ".join(x.dropna().astype(str).values.squeeze(axis=-1)))
        aux_df_notify_tramo.name = "motivo"
    else:
        aux_df_notify_tramo = None

    return aux_df_notify_tramo


def process_results(df_registro, df_detectado, df_revisar):
    # despues de realizar la detección, esta función formateará las detecciones correctamente para resgistrar los resultados
    # actualizar los revisar
    res1 = df_registro.drop(columns=["status"])
    res1.loc[:, "time"] = res1.index
    res2 = (~ res1.apply(tuple, 1).isin(df_revisar.reset_index()[res1.columns].apply(tuple, 1))) & (df_registro["status"] == "revisar")  # lo de columns es muy importante porque el orden es relevante
    df_registro.loc[res2, "status"] = "Done"

    # de cuales notificar
    df_detectado.loc[:, "time"] = df_detectado.index
    res_aux = df_detectado.apply(tuple, 1).isin(df_registro.drop(columns="status").reset_index()[df_detectado.columns].apply(tuple, 1))
    df_detectado = df_detectado.drop(columns="time")
    notify_aux = df_detectado.loc[~res_aux]
    notify_aux = notify_aux.dropna()
    if notify_aux.size > 0:
        df_notify = notify_aux
    else:
        df_notify = None

    # cuales hay que revisar (flag status=revisar)
    new_df_hist = df_detectado.copy()
    new_df_hist = new_df_hist.merge(df_registro, on=["time", "motivo", "id", "tables2check"], how="left")
    new_df_hist.loc[:, "status"] = new_df_hist["status"].fillna("revisar")
    df_revisar = new_df_hist.loc[new_df_hist.loc[:, "status"] == "revisar"].drop(columns="status").copy()

    return new_df_hist, df_notify, df_revisar
