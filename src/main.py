# ==========================================================
# Script creado por Arturo Sirvent (Junio 2023, y modificado Julio 2023) para el proyecto Smart City Valencia
# El proceso es el siguiente:
#   1. Se cargan los datos de cada tabla que se vaya a procesar (cada entrada en el objeto json tables2check) (los datos de entrada deberan ser time, valor y el id)
#   2. Se realiza una detección de nans, valores multiples y valores anomalos sobre esos datos concretos (para cada id)
#   3. Los resultados son fechas y el motivo de la alerta/tipo de anomalía (para cada ID).
#   4. Se juntan todas las detecciones de todos los IDs de todas las tablas ,y se guarda en una tabla de detecciones.
#   5. Además tenemos un .csv donde tenemos los registros a revisar, y cuando los eliminamos ahí, se marcarán como "revisado" o "Done" en la tabla de registro
# ==========================================================


# importacion de librerias
import json
import sys

import pandas as pd

from utils import read_from_db, write_to_db, Email, process_results, procesado_datos, EvalTramo_Zscore, detect_id_main


# declaracion de entorno y directorios

entorno = sys.argv[1]  # tendrá valores: PRE o PRO
#entorno="LOCAL"

directorio = f"/opt/etls/sc_vlci/{entorno}/py_oci_trafico_deteccion_anomalias"
#directorio ="/home/arsifre/git/deteccionanomaliastrafico"

PATH_CONFIG_DIR = f"{directorio}/config"
PATH_REVISAR_CSV = f"{directorio}/data/revisar.csv"


THRESH = 4.5 #Parámetros que controla la permisividad del umbral. Más significa mayor umbral (se nos pueden colar más anomalías sin detectar).

# carga de las config y los secrets
with open(f"{PATH_CONFIG_DIR}/config.json") as json_file:
    config_file = json.load(json_file)

with open(f"{PATH_CONFIG_DIR}/secrets.json") as json_file:
    secrets_file = json.load(json_file)

# listas de las tablas que revisar
tables2check = list(config_file["tables2check"].keys())
assert len(tables2check) > 0, "No hay tablas que revisar en el fichero config"

# loop por las diferentes tablas
full_list_tablas = []
for i in tables2check:
    print(f" \t Procesando {i}" + "\n" + "=="*23 + "\n")

    # ahora se cargan los datos, se procesan y se le pasan al modulo de deteccion
    config_tabla_aux = config_file["tables2check"][i]
    secretos_tabla_aux = secrets_file["database_secrets"][config_tabla_aux["database_secrets_id"]][entorno]
    df_aux = read_from_db(secretos_tabla_aux, config_tabla_aux["query"])
    # si tiene ID, recorremos los ids, y si no lo tiene no los recorremos (por ahora no esta soportado que no tengan ids)
    df_procesado = procesado_datos(df_aux)

    # ahora recorremos los IDs
    IDS = df_procesado["id"].unique()
    list_final_notificar = []

    for id in IDS:
        #loop sobre los diferentes ids que haya, por ejemplo tramos, pero podrían ser rutas de la EMT, etc...
        df_ID = df_procesado.loc[df_procesado["id"] == id].drop(columns=["id"])
        # pasamos cada conjunto de los datos a una funcion que va a confeccionar el df final.
        # tambien le pasamos una funcion con un método classify que nos devuelve un pd Series con 0 o 1 segun si la fecha del indice (UTC)
        # es o no considerada anomalía

        # ===================== DECLARACIÓN DEL MODELO =====================
        modelo = EvalTramo_Zscore  # aqui pondremos el modelo que sea, lo único que debe devolver es un pd Series con indice fechas y valores de 1 o 0 según si es o no anomalía
        # el modelo que solo se define con el df, y los argumentos para el metodo classify que recibe el resto de parámetros que pudieran hacer falta
        # ===================== ======================= =====================

        #recorremos cada ID en busca de anomalías, usando el modelo definido
        df_final_aux = detect_id_main(df_ID, id, modelo, thresh=THRESH, type="mean")
        if df_final_aux is not None:
            # si hubiera duplicados vamos a informar, pero seguimos adelante
            df_final_aux = df_final_aux.to_frame()
            df_final_aux["id"] = id
            aux_duplicados = (df_final_aux.reset_index()).duplicated()
            if aux_duplicados.any(axis=None):
                print(f"----- Hay elementos duplicados en la lista de notificaciones, para el ID:  {id} ----- \n \t {aux_duplicados}")
                df_final_aux = df_final_aux.reset_index().drop_duplicates(keep="first").set_index("time")
            list_final_notificar.append(df_final_aux)

    # si la lista no esta vacía, lo concatenams
    if len(list_final_notificar) > 0:
        final_df_notificar = pd.concat(list_final_notificar)
        final_df_notificar["tables2check"] = i
    else:
        pass
    full_list_tablas.append(final_df_notificar)

# concatenamos todo
full_df = pd.concat(full_list_tablas)


# sacamos el registro de todas las tablas e id's
query_registro_aux = f'SELECT * FROM {config_file["tables2write"][config_file["tables2check"][i]["tables2write"]]["table_name"]}'
df_registro_aux = read_from_db(secretos_tabla_aux, query_registro_aux)
df_registro_aux.index = pd.to_datetime(df_registro_aux["time"], utc=True)
df_registro_aux = df_registro_aux.drop(columns="time")

# carga de los datos csv con los registros que se han revisado
try:
    # este no tiene la columna de status
    df_revisar = pd.read_csv(PATH_REVISAR_CSV, index_col="time")
    df_revisar.index = pd.to_datetime(df_revisar.index, utc=True)
except Exception as e:
    # si no se puede cargar, lo creamos vacio
    print("Error cargando el df revisar")
    df_revisar = pd.DataFrame(columns=["motivo", "id", "tables2check"], index=pd.Index([], name="time"))
    df_revisar.index = pd.to_datetime(df_revisar.index, utc=True)

# el procesado de resultados es tomar los resultados + el registro + los datos revisados
# y con ello: miramos los nuevos registros que hemos detectado y los cuales no estaban en el registros anterior
# estos son devueltos como df_notify, porque tenemos que dar una alerta por ellos.
# el nuevo registro será lo que hemos detectado en esta ejecución (la anterior es descartada) y se actualiza la columna
# status, en aquellos registros que hayan sido borrados del fichero revisar.csv
new_df_hist, df_notify, new_df_revisar = process_results(df_registro_aux, full_df, df_revisar)

# una vez se tiene, se guarda en la vase de datos, y si no se puede guardar, que falle, porque sino no despertará las alarmas de que algo malo pasa
write_to_db(new_df_hist, secrets_file["database_secrets"]["vlci2"][entorno], "t_datos_python_trafico_deteccionanomalias")

#guardar en el fichero los registros a revisar (se sobre escribe cada vez)
new_df_revisar.to_csv(PATH_REVISAR_CSV)


# Si hay aglo que notificar, confeccionamos el email y lo mandamos
if df_notify is not None:

    print("Enviando email...")
    
    email = Email(secrets_file["email_secrets"][entorno], config_file["tables2check"])
    email.send_email(f"[{entorno}] Proceso de detección de anomaías - Informe job 'py_trafico_anomaly_detection'",email.write_message_from_df(df_notify))
    #print(f"[{entorno}] Proceso de detección de anomaías - Informe job 'py_trafico_anomaly_detection'", email.write_message_from_df(df_notify))
    print("Email enviado")
else:
    print("No hay email que enviar")
